import 'dart:convert';
import 'dart:io';

import 'package:string_matching/string_match.dart';
import 'package:string_matching/src/models/product.dart';
import 'package:string_matching/src/print_result.dart';

void main() async {
  while (true) {
    print('Enter search input:');
    String userInput = stdin.readLineSync(encoding: Encoding.getByName('utf-8'));
    print('=' * 125);

    List<Product> results = await getMatches(userInput);

    printResults(results);
  }
}
