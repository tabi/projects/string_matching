import 'package:string_matching/src/edit_distance.dart';
import 'package:string_matching/src/load_csv.dart';
import 'package:string_matching/src/models/product.dart';

Future<List<Product>> getMatches(String userInput) async {
  final List<Product> products = await loadCsv();
  final List<Product> results = [];

  //Compute edit distance between user input and all product names
  products.forEach((Product product) {
    product.editDistance = getEditDistance(product.name, userInput);
  });

  //Sort over 3 values, in this order: edit distance, word length, frequency
  products.sort((a, b) {
    int editDistanceRank = a.editDistance.compareTo(b.editDistance);
    if (editDistanceRank != 0) {
      return editDistanceRank;
    } else {
      int wordLengthRank = a.name.length.compareTo(b.name.length);
      if (wordLengthRank != 0) {
        return wordLengthRank;
      } else {
        int frequencyRank = b.frequency.compareTo(a.frequency);
        return frequencyRank;
      }
    }
  });

  //Take one example per category to show, take a maximum of 20 examples
  List<String> _categories = [];
  for (Product product in products) {
    if (_categories.length <= 20) {
      if (_categories.contains(product.category) == false) {
        _categories.add(product.category);
        results.add(product);
      }
    }
  }
  return results;
}
