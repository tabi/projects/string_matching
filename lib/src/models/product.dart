import 'package:meta/meta.dart';

class Product {
  final String code;
  final int frequency;
  final String name;
  final String category;
  
  double editDistance;

  Product({
    @required this.code,
    @required this.frequency,
    @required this.name,
    @required this.category,
  });
}
