import 'package:dolumns/dolumns.dart';
import 'package:string_matching/src/models/product.dart';

void printResults(List<Product> results) {
  List<List<String>> _printColumns = [];
  _printColumns.add(['Product Name', 'Category', 'Frequency', 'Edit Distance']);
  results.forEach((Product product) {
    _printColumns.add(['${product.name}', '${product.category}', '${product.frequency}', '${product.editDistance}']);
  });
  print(dolumnify(_printColumns, columnSplitter: ' | ', headerIncluded: true, headerSeparator: '='));
  print('\n');
}
