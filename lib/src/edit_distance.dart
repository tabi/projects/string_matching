import 'dart:math';
import 'package:edit_distance/edit_distance.dart';

double getEditDistance(String productName, String userInput) {
  JaroWinkler jaroWinkler = JaroWinkler();
  double jaroScore = 0;

  productName = productName.toLowerCase();
  userInput = userInput.toLowerCase();

  if (userInput.contains(' ')) {
    jaroScore = jaroWinkler.normalizedDistance(userInput, productName);
  } else {
    List<double> jaroWordScores = [];
    for (String productWord in productName.split(' ')) {
      double jaroWordScore = jaroWinkler.normalizedDistance(userInput, productWord);
      jaroWordScores.add(jaroWordScore);
    }
    jaroScore = jaroWordScores.reduce(min);
  }
  return jaroScore;
}
