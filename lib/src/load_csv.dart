import 'dart:io';
import 'dart:convert';

import 'package:string_matching/src/models/product.dart';

Future<List<Product>> loadCsv() async {
  final List<Product> products = [];
  File file = File('assets/tblProduct_de.csv');

  final Stream<String> lines = file.openRead().transform(utf8.decoder).transform(LineSplitter());

  await for (String line in lines) {
    final List<String> values = line.split(';');
    products.add(Product(
      code: values[0],
      frequency: int.parse(values[1]),
      name: values[2],
      category: values[3],
    ));
  }
  return products;
}
